package com.health.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.health.entity.Insurance;

@Repository
public interface InsuranceRepository extends JpaRepository<Insurance, Integer> {
	List<Insurance> findByAgeGroup(String ageGroup);
	List<Insurance> findByAgeGroupAndSalaryGroup(String ageGroup, String salaryGroup);
}
