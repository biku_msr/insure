package com.health.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.health.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	User findByUniqueId(String uniqueId);
}
