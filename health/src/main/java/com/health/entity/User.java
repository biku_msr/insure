package com.health.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer userId;
	private String uniqueId;
	private String likedInsurance;
	private String name;
	
	public User() {
		super();
	}

	public User(Integer userId, String uniqueId, String likedInsurance, String name) {
		super();
		this.userId = userId;
		this.uniqueId = uniqueId;
		this.likedInsurance = likedInsurance;
		this.name = name;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getLikedInsurance() {
		return likedInsurance;
	}

	public void setLikedInsurance(String likedInsurance) {
		this.likedInsurance = likedInsurance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
}
