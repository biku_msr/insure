package com.health.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.health.entity.Insurance;
import com.health.service.InsuranceService;

@RestController
@RequestMapping("/fetch")
public class ImageController {
	@Autowired
	private InsuranceService insuranceService;

	/*@PostMapping("/upload")
	public String returnDetails(@RequestParam("file") MultipartFile file){
		//return "true";
		return insuranceService.ImageResponse(file);


	}*/

	/*@GetMapping("/insurance/{ageGroup}")
	public ResponseEntity<List<Insurance>> getInsuranceByAge(@PathVariable("ageGroup") String ageGroup){
		if(null != ageGroup){
			List<Insurance> insuranceDisplayed = insuranceService.insurancebyAge(ageGroup);
			if(null != insuranceDisplayed){
				return new ResponseEntity<List<Insurance>>(insuranceDisplayed, HttpStatus.OK);
			}

			else
				return new ResponseEntity<List<Insurance>>(HttpStatus.NOT_FOUND);

		}
		else
			return new ResponseEntity<List<Insurance>>(HttpStatus.NO_CONTENT);
	}*/
	
	@GetMapping("/insurance/{ageGroup}/{salaryGroup}")
	public ResponseEntity<List<Insurance>> getInsuranceByAgeAndAgeGroup(@PathVariable("ageGroup") String ageGroup,
			@PathVariable("salaryGroup") String salaryGroup){
		List<Insurance> insuranceDisplayed = new ArrayList<>();
		if(!"0".equals(salaryGroup)){
			insuranceDisplayed = insuranceService.insurancebyAgeAndSalary(ageGroup, salaryGroup);
			if(null != insuranceDisplayed){
				return new ResponseEntity<List<Insurance>>(insuranceDisplayed, HttpStatus.OK);
			}
			else
				return new ResponseEntity<List<Insurance>>(HttpStatus.NOT_FOUND);
		}
		else
			insuranceDisplayed = insuranceService.insurancebyAge(ageGroup);
			return new ResponseEntity<List<Insurance>>(insuranceDisplayed, HttpStatus.OK);
	}
	
}