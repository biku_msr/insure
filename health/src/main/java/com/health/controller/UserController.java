package com.health.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.health.entity.User;
import com.health.service.UserService;

@RestController
@RequestMapping("/login")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/user")
	public ResponseEntity<User> getUserDetails(@RequestBody User user){
		if(null != user){
			User userServ = userService.getUserDetails(user);
			if(null != userServ){
				return new ResponseEntity<User>(userServ, HttpStatus.OK);
			}
			else
				return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}else
			return new ResponseEntity<User>(HttpStatus.NO_CONTENT); 
	}
	
	@PostMapping("/user/{uniqueId}")
	public ResponseEntity<User> getLawyer(@PathVariable("uniqueId") String uniqueId,@RequestBody String insuranceId){
		if(null != insuranceId && null != uniqueId){
			User userServ = userService.getUserLike(Integer.parseInt(insuranceId.substring(0,insuranceId.length()-1)),uniqueId);
			if(null != userServ){
				return new ResponseEntity<User>(userServ, HttpStatus.OK);
			}
			else
				return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}else
			return new ResponseEntity<User>(HttpStatus.NO_CONTENT); 
	}

}
